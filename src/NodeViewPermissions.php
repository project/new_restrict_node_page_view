<?php

namespace Drupal\restrict_node_page_view;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\NodeType;

/**
 * NodeViewPermissions.
 *
 * @copyright Copyright © 2020 gladd.gr. All rights reserved.
 */
class NodeViewPermissions {

  use StringTranslationTrait;

  /**
   * Add custom permissions for node accesss.
   *
   * @return array
   *   permissions array
   */
  public function viewPermissions() {
    $permissions = [];

    $nodetypes = NodeType::loadMultiple();

    foreach ($nodetypes as $type => $object) {
      $permissions['view any node ' . $type . ' page'] = [
        'title' => $this->t('View full node %nodetype page', ['%nodetype' => $type]),
        'description' => $this->t('Be able to access %nodetype node page', ['%nodetype' => $type]),
      ];
    }

    return $permissions;
  }

}
